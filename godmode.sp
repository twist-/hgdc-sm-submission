#include <sourcemod>
#include <sdktools>
#include <colorlib>
#include <clientprefs>
#include <sdkhooks>

#pragma semicolon 1
#pragma newdecls required

bool g_bGodMode[MAXPLAYERS + 1];
bool g_bAlreadySlayedAll[MAXPLAYERS + 1];

Handle g_hGodMode = INVALID_HANDLE;

public Plugin myinfo =
{
	name = "hgdc godmode",
	author = "Twist",
	description = "",
	version = "1.0",
	url = ""
};

public void OnPluginStart()
{	
	RegConsoleCmd("sm_godmode", cmdGod);
	HookEvent("player_spawn",SpawnEvent);   
	g_hGodMode = RegClientCookie("GodMode", "God Mode", CookieAccess_Protected);
}
public Action cmdGod(int client, int args)
{
    char option1[32];
    Format(option1, sizeof(option1), "Godmode: %s", g_bGodMode[client] ?"[ON] OFF":"ON [OFF]");
    Menu menu = new Menu(Menu_Callback);
    menu.SetTitle("GodMode Menu");
    menu.AddItem("option1", option1);
    menu.AddItem("option2", "Kill All Others");
    menu.Display(client, 30);
    return Plugin_Handled;
}
public void OnClientDisconnect(int client)
{
	g_bAlreadySlayedAll[client] = false;
}
public Action SpawnEvent(Event event,char[] name,bool dontBroadcast)
{
    int client_id = GetEventInt(event, "userid");
    int client = GetClientOfUserId(client_id);
    SDKHook(client, SDKHook_OnTakeDamage, Hook_OnTakeDamage);
}
public Action Hook_OnTakeDamage( int victim, int &attacker, int &inflictor, float &damage, int &damagetype )
{
    if(g_bGodMode[victim])
    {
        damage = 0.0;
        return Plugin_Changed;
    }   
    return Plugin_Continue;
}
public int Menu_Callback(Menu menu, MenuAction action, int param1, int param2)
{
	switch (action) {
		case MenuAction_Select: 
		{
			char item[32];
			menu.GetItem(param2, item, sizeof(item));

			if(StrEqual(item, "option1")) 
			{
				int client = param1;	 
				char sValue[8];
				g_bGodMode[client] = !g_bGodMode[client]; 
				IntToString(g_bGodMode[client], sValue, sizeof(sValue)); 
				SetClientCookie(client, g_hGodMode, sValue); 
				CPrintToChat(client, "[{green}GodMode{default}] Godmode has been %s{default}!", g_bGodMode[client] ?"{green}Enabled":"{red}Disabled");
				if(!g_bAlreadySlayedAll[client] && g_bGodMode[client]) 
				{
					CPrintToChatAll("[{green}GodMode{default}] {red}A GOD HAS ENTERED THE BATTLEGROUND!!!");
					g_bAlreadySlayedAll[client] = true;
					for(int i = 1; i <= MaxClients; i++)
					{
						if(i != client) 
						{
							ForcePlayerSuicide(i);
						}
					}
				}
			}
			else if(StrEqual(item, "option2"))
			{
				int client = param1;	 
				CPrintToChat(client, "[{green}GodMode{default}] Everyone has been killed!");
				for(int i = 1; i <= MaxClients; i++)
				{
					if(i != client) {
						ForcePlayerSuicide(i);
					}
				}
			}
		}
		case MenuAction_End:
		{
			delete menu;
		}
	}
}